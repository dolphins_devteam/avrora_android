package com.avrora.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avrora.app.R;
import com.avrora.tabs.ProfileActivity;

import java.util.ArrayList;
import java.util.HashMap;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;


public class BonusesAdapter extends BaseAdapter {

    private ProfileActivity context;
    private ArrayList<HashMap<String, String>> titleList;

    public BonusesAdapter(ProfileActivity context, ArrayList<HashMap<String, String>> titleList) {
        super();
        this.context = context;
        this.titleList = titleList;
    }

    @Override
    public int getCount() {
        return titleList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        BonusesAdapter.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(context.getActivity());

        if (convertView == null) {
            viewHolder = new BonusesAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.bonus_item, parent, false);

            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
            viewHolder.bonusCount = (TextView) convertView.findViewById(R.id.bonus_counts);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (BonusesAdapter.ViewHolder) convertView.getTag();
        }

        if (Double.parseDouble(titleList.get(position).get("sum")) > 0) {
            viewHolder.bonusCount.setTextColor(GREEN);
            viewHolder.imageView.setImageResource(R.drawable.plus_circle);
            viewHolder.bonusCount.setText("+" + titleList.get(position).get("sum"));

        } else {
            viewHolder.bonusCount.setTextColor(RED);
            viewHolder.imageView.setImageResource(R.drawable.minus_circle);
            viewHolder.bonusCount.setText(titleList.get(position).get("sum"));
        }

        viewHolder.date.setText(titleList.get(position).get("date"));

        return convertView;
    }

    private class ViewHolder {
        TextView bonusCount;
        TextView date;
        ImageView imageView;
    }
}
