package com.avrora.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.avrora.tabs.ProductMarkActivity;
import com.avrora.tabs.ProfileActivity;
import com.avrora.tabs.ShopMarkActivity;


public class FragmentAdapterClass extends FragmentPagerAdapter {

    private int tabCount;

    public FragmentAdapterClass(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ProfileActivity();
            case 1:
                return new ProductMarkActivity();
            case 2:
                return new ShopMarkActivity();
            default:
                throw new Error("Unreachable state");
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }

}