package com.avrora.utils;

/**
 * Created by egornoskov on 08.05.17.
 */

public class Constants {

    //public static final String HOST = "http://37.57.101.228/api/";

    public static final String HOST = "http://mobile-app.avrora-shop.com.ua/api/";

    public static final String AUTORIZATION="login?";


    public static final String AUTHENTICATION="login-confirm?";

    public static final String SEND_GEO="send-geo?";

    public static final String GET_SHOP_LIST="get-shop-list?";

    public static final String SET_PRODUCT_RATING="set-product-rating?";

    public static final String SET_SHOP_RATING="set-shop-rating?";


    public static final String GET_PROFILE="get-profile?";

    public static final String GET_BONUS_LIST="get-bonus-list?";

    //public static final int LOCATION_INTERVAL =60*1000;//1 minute

    public static final int SQLITE_MAX_SIZE =104857600;//100 megabytes in bytes

    public static final float LOCATION_DISTANCE = 0;

    public static final int REQUEST_CAMERA = 0;

    public static final int SELECT_FILE = 1;


}
