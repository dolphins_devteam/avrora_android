package com.avrora.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.avrora.app.R;


public class InternetCheck {

    public static void showNoInternetDialog(final Context context, final Activity activity){

        final android.app.AlertDialog.Builder builder =
                new android.app.AlertDialog.Builder(context);

        builder.setMessage(context.getString(R.string.internet_error));

        builder.setNegativeButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }
        );

        builder.create().show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        Log.d("isOnline", String.valueOf((netInfo != null && netInfo.isConnectedOrConnecting())));
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
