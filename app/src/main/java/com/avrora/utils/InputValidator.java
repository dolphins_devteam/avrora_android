package com.avrora.utils;

/**
 * Created by egornoskov on 23.02.17.
 */
import android.text.TextUtils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

public class InputValidator {
    /**
     * @param countryCode         String like '41'
     * @param nationalPhoneNumber String like '446681800' or '0446681800'
     * @return {@code null} if there was an exception during parsing, {@code Phonenumber.PhoneNumber} instance otherwise
     */
    public static Phonenumber.PhoneNumber parsePhoneNumberByGoogle(String countryCode, String nationalPhoneNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
        Phonenumber.PhoneNumber phoneNumber = null;
        try {
            phoneNumber = phoneNumberUtil.parse(nationalPhoneNumber, isoCode);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }

        return phoneNumber;
    }

    /**
     * @param phoneNumber Valid phone number
     * @return String like '+41446681800'
     */
    public static String getE164PhoneFormat(Phonenumber.PhoneNumber phoneNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        return phoneNumberUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
    }

    /**
     * Validate a phone number using 'libphonenumber' library
     */
    public static boolean isPhoneNumberValidByGoogle(Phonenumber.PhoneNumber phoneNumber) {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        return phoneNumberUtil.isValidNumber(phoneNumber);
    }

    public static boolean isEmailValid(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isPasswordValid(CharSequence target) {
        return target.length() >= 6;
    }

    public static boolean isFieldEmpty(CharSequence target) {
        return TextUtils.isEmpty(target);
    }

    /**
     * Check whether the given String contains any whitespace characters.
     *
     * @param str the String to check
     * @return <code>true</code> if the String is not empty and contains at least 1 whitespace character
     * @see Character#isWhitespace
     */
    public static boolean containsWhitespace(String str) {
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}

