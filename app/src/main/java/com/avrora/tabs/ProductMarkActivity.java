package com.avrora.tabs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.avrora.app.R;
import com.avrora.utils.InputValidator;
import com.avrora.utils.Utility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.techery.properratingbar.ProperRatingBar;

import static com.avrora.activity.AuthActivity.USER_ID;
import static com.avrora.activity.AuthActivity.pref;
import static com.avrora.db.DB.readFromDBtoAdressArray;
import static com.avrora.db.DB.readFromDBtoShopArray;
import static com.avrora.utils.Constants.HOST;
import static com.avrora.utils.Constants.REQUEST_CAMERA;
import static com.avrora.utils.Constants.SELECT_FILE;
import static com.avrora.utils.Constants.SET_PRODUCT_RATING;
import static com.avrora.utils.InternetCheck.isOnline;
import static com.avrora.utils.InternetCheck.showNoInternetDialog;
import static com.avrora.utils.Utility.decodeSampledBitmapFromFile;
import static com.avrora.utils.Utility.getPath;
import static com.avrora.utils.Utility.hideSoftKeyboard;
import static com.avrora.utils.Utility.md5Custom;
import static com.avrora.utils.Utility.modifyOrientation;
import static com.avrora.utils.Utility.showSoftKeyboard;

public class ProductMarkActivity extends Fragment{

    private EditText feedbackEdtx;
    private EditText textProductEdtx;
    //private TextView textProductHint;

    private ProperRatingBar productRate;
    private Spinner spinnerAdress;
    private ArrayList<String> addressList;
    private String adress="";
    private String product;
    private String feedbackStr;
    private ImageView imagePreview;
    private ImageView addImage;
    private FrameLayout plusLayout;
    private FrameLayout minusLayout;


    private String userChoosenTask;
    public static final String TAG="ProductMarkActivity";
    public String shopIdSelect;
    public int selectedShopPosition;

    public String rateStr;
    public String encodedImage;
    private String userImagePath;
    private Bitmap userImage;
    private FrameLayout loader;
    private String estimateStatusProduct="";
    private ScrollView productMarkLayout;
    private RelativeLayout feedBackContainer;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_product_mark, container, false);
        //Log.d(TAG,"adressList"+adressList);

        loader=(FrameLayout) view.findViewById(R.id.loader_container);

        addressList = new ArrayList<>();
        addressList=readFromDBtoAdressArray(getContext());


        productMarkLayout = (ScrollView) view.findViewById(R.id.product_mark_layout);
        productMarkLayout.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent ev)
            {
                if(getActivity()!=null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm.isAcceptingText()) {
                        hideSoftKeyboard(getActivity());
                        //Log.d(TAG,"Software Keyboard was shown");
                    } else {
                        //Log.d(TAG, "Software Keyboard was not shown");
                    }
                }
                return false;
            }
        });

        plusLayout=(FrameLayout) view.findViewById(R.id.plus_layout);


        plusLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        minusLayout=(FrameLayout) view.findViewById(R.id.minus_layout);

        minusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userImage=null;
                addImage.setVisibility(View.VISIBLE);
                plusLayout.setVisibility(View.VISIBLE);
                imagePreview.setVisibility(View.GONE);
                minusLayout.setVisibility(View.GONE);

            }
        });

        addImage=(ImageView)view.findViewById(R.id.add_photo);

        imagePreview = (ImageView) view.findViewById(R.id.image_preview);

        feedbackEdtx = (EditText) view.findViewById(R.id.feedback_edit_text);

        //textProductHint = (TextView) view.findViewById(R.id.text_product_hint);

        feedbackEdtx = (EditText) view.findViewById(R.id.feedback_edit_text);

        textProductEdtx = (EditText) view.findViewById(R.id.text_product_edit_text);
        textProductEdtx.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    // position the text type in the left top corner
                    textProductEdtx.setGravity(Gravity.LEFT );
                    textProductEdtx.setCursorVisible(true);

                }else{
                    // no text entered. Center the hint text.
                    textProductEdtx.setGravity(Gravity.CENTER);
                    textProductEdtx.setHint(getString(R.string.product_code));
                    textProductEdtx.setCursorVisible(false);
                }
            }
        });
        textProductEdtx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textProductEdtx.setFocusableInTouchMode(true);
                textProductEdtx.requestFocus();
            }
        });

        textProductEdtx.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(textProductEdtx.hasFocus()) {
                    textProductEdtx.setCursorVisible(true);
                    textProductEdtx.setGravity(Gravity.LEFT);
                    textProductEdtx.setHint("");
                    if(getActivity()!=null){
                        showSoftKeyboard(getActivity());

                    }
                }
            }
        });

        productRate = (ProperRatingBar) view.findViewById(R.id.mark_stars);

        Button estimateButton = (Button) view.findViewById(R.id.estimateButton);
        estimateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isOnline(getContext())){
                    submitProductEstimate();
                }else {
                    showNoInternetDialog(getContext(),getActivity());

                }

            }
        });

        spinnerAdress = (Spinner) view.findViewById(R.id.spinner_shop);

        if (getActivity()!=null&&getContext()!=null) {
            if (readFromDBtoAdressArray(getContext()) != null && readFromDBtoShopArray(getContext())!=null) {
                //loader.setVisibility(View.GONE);

                ArrayAdapter<String> adapterShop = new ArrayAdapter<String>(getActivity(), R.layout.custom_spiner_item, R.id.textView, addressList);
                // Log.d(TAG,"adressList "+adressList);

                spinnerAdress.setAdapter(adapterShop);
                spinnerAdress.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        adress = readFromDBtoAdressArray(getContext()).get(position);

                        //shopIdSelect = shopArrayList.get(position).get("id");
                        shopIdSelect = readFromDBtoShopArray(getContext()).get(position).get("id");
                        selectedShopPosition = position;
                        Log.d(TAG, "shopIdSelect: " + shopIdSelect);
                        Log.d(TAG, "onSelect: selectedShopPosition: " + selectedShopPosition);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }

        feedBackContainer=(RelativeLayout)view.findViewById(R.id.feedback_container);
        feedBackContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbackEdtx.requestFocus();
                if(getActivity()!=null){
                    showSoftKeyboard(getActivity());

                }
            }
        });
        return view;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_IMAGE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals(getString(R.string.take_photo)))
                        cameraIntent();
                    else if(userChoosenTask.equals(getString(R.string.choose_from_library)))
                        galleryIntent();
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (userImagePath != null && !userImagePath.isEmpty()) {
            outState.putString("userImagePath", userImagePath);
            outState.putString("product",textProductEdtx.getText().toString());
            outState.putString("feedback",feedbackEdtx.getText().toString());
            outState.putString("shopIdSelect",shopIdSelect);
            outState.putInt("selectedShopPosition",selectedShopPosition );

            Log.d(TAG,"onSaveInstanceState.shopIdSelect: "+shopIdSelect);


            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("userImagePath")) {
                userImagePath = savedInstanceState.getString("userImagePath");
                userImage = getUserImage(userImagePath);

                addImage.setVisibility(View.GONE);
                plusLayout.setVisibility(View.GONE);
                imagePreview.setVisibility(View.VISIBLE);
                imagePreview.setImageBitmap(userImage);
                minusLayout.setVisibility(View.VISIBLE);
            }

            if(savedInstanceState.containsKey("product")){
                product=savedInstanceState.getString("product");

                textProductEdtx.setText(product);
            }

            if(savedInstanceState.containsKey("feedback")){
                feedbackStr=savedInstanceState.getString("feedback");

                feedbackEdtx.setText(feedbackStr);
            }

            if(savedInstanceState.containsKey("shopIdSelect")){
                shopIdSelect=savedInstanceState.getString("shopIdSelect");
                Log.d(TAG, "onViewStateRestored, shopIdSelect: " + shopIdSelect);
            }

            if(savedInstanceState.containsKey("selectedShopPosition")){
                selectedShopPosition=savedInstanceState.getInt("selectedShopPosition");
                Log.d(TAG, "onViewStateRestored, selectedShopPosition: " + selectedShopPosition);
                spinnerAdress.setSelection(selectedShopPosition);
            }
        }

        super.onViewStateRestored(savedInstanceState);
    }

    private void selectImage() {
        final CharSequence[] items = { getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.back_string) };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.add_image_title));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result=Utility.checkPermission(getContext());

                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask =getString(R.string.take_photo);
                    if(result)
                        cameraIntent();

                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.back_string))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory()+File.separator + "image.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                userImagePath = Environment.getExternalStorageDirectory() + File.separator + "image.jpg";
                userImage = getUserImage(userImagePath);

                addImage.setVisibility(View.GONE);
                plusLayout.setVisibility(View.GONE);
                imagePreview.setVisibility(View.VISIBLE);
                imagePreview.setImageBitmap(userImage);
                minusLayout.setVisibility(View.VISIBLE);
            }
        }
    }


    private Bitmap getUserImage(String userImagePath){
        File file = new File(userImagePath);
        Bitmap bm = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);
        try {
            bm = modifyOrientation(bm, file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bm;
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        String path=getPath(getContext(),data.getData());
        assert path != null;
        String type= path.substring(path.lastIndexOf(".")+1);
        Log.d(TAG, "Type: "+type);
        if(type.equals("jpg")||type.equals("png")||type.equals("bmp")
                ||type.equals("jpeg")||type.equals("webp")||type.equals("gif")) {

            // if (data != null) {
            // bm = BitmapFactory.decodeFile(getPath(getContext(),data.getData()), getOptions());
            Bitmap bm = decodeSampledBitmapFromFile(getPath(getContext(), data.getData()), 1000, 700);


            // }
            addImage.setVisibility(View.GONE);
            plusLayout.setVisibility(View.GONE);
            imagePreview.setVisibility(View.VISIBLE);
            try {
                //assert data != null;
                userImage = modifyOrientation(bm, getPath(getContext(), data.getData()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            imagePreview.setImageBitmap(userImage);
            minusLayout.setVisibility(View.VISIBLE);

        }else{
            Toast.makeText(getContext(),getString(R.string.select_image_error),Toast.LENGTH_LONG).show();
        }
    }

    public void submitProductEstimate() {


        feedbackStr = feedbackEdtx.getText().toString();
        product = textProductEdtx.getText().toString();
        rateStr= String.valueOf(productRate.getRating());

        feedbackEdtx.setError(null);
        textProductEdtx.setError(null);


        boolean cancel = false;
        View focusView = null;

        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
        if(userImage != null) {
            userImage.compress(Bitmap.CompressFormat.JPEG, 100, baos1);
            byte[] b1 = baos1.toByteArray();
            encodedImage = Base64.encodeToString(b1, Base64.DEFAULT);
            Log.d(TAG,"encodedImage: "+encodedImage);
            Log.d(TAG,"encodedImage.length: "+encodedImage.length());
        }

        if(adress.equals("")||adress.equals(getString(R.string.adress))){
            Toast.makeText(getActivity(),getString(R.string.check_shop_adress),Toast.LENGTH_LONG).show();
            cancel = true;

        }

        if (InputValidator.isFieldEmpty(product)) {
            textProductEdtx.setFocusableInTouchMode(true);
            textProductEdtx.setError(getString(R.string.error_field_required));
            focusView = textProductEdtx;
            cancel = true;
        }if (cancel) {
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            loader.setVisibility(View.VISIBLE);

            new productEstimateAsync().execute();

        }

    }

    private  class productEstimateAsync extends AsyncTask<Void, Void, Void> {
        String error="";

        @Override
        protected Void doInBackground(Void... voids) {


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HOST+SET_PRODUCT_RATING);

            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

            nameValuePair.add(new BasicNameValuePair("user_id", pref.getString(USER_ID,null)));
            nameValuePair.add(new BasicNameValuePair("shop_id", shopIdSelect));
            nameValuePair.add(new BasicNameValuePair("product_name",product));
            nameValuePair.add(new BasicNameValuePair("comment", feedbackStr));
            nameValuePair.add(new BasicNameValuePair("rate", rateStr));
            nameValuePair.add(new BasicNameValuePair("hash", md5Custom(pref.getString(USER_ID,null)+
                    shopIdSelect+product+feedbackStr+rateStr)));
            nameValuePair.add(new BasicNameValuePair("image", encodedImage));
            //Log.d(TAG, "mainToPost. encodedImage.lenght: " + encodedImage.length()); // todo time to time we have NULL here

            Log.d(TAG, "mainToPost" + nameValuePair.toString());

            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));
                HttpResponse response = httpclient.execute(httppost);
                String responseString;
                HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
                    responseString = EntityUtils.toString(responseEntity);
                    Log.d(TAG,"response"+ responseString);
                    JSONObject jObject  = new JSONObject(responseString);
                    estimateStatusProduct=jObject.getString("status");

                }
                int status = response.getStatusLine().getStatusCode();
                Log.d(TAG,"Status:"+ String.valueOf(status));


            } catch (org.apache.http.conn.HttpHostConnectException |org.apache.http.conn.ConnectTimeoutException e){
                error=getString(R.string.server_error);

            } catch (IOException | JSONException e) {
                //z Toast.makeText(getApplication(),getString(R.string.json_parse_error),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.GONE);
            if(getActivity()!=null&&getContext()!=null) {

                if (error.equals(getString(R.string.server_error))) {
                    Toast.makeText(getContext(), getString(R.string.server_error), Toast.LENGTH_LONG).show();
                } else {
                    if (estimateStatusProduct.equals("ok")) {
                        Toast.makeText(getContext(), getString(R.string.estimate_thanks), Toast.LENGTH_LONG).show();

                        feedbackEdtx.setText("");
                        textProductEdtx.setText("");
                        addImage.setVisibility(View.VISIBLE);
                        plusLayout.setVisibility(View.VISIBLE);
                        imagePreview.setVisibility(View.GONE);
                        //textProductHint.setVisibility(View.VISIBLE);
                        minusLayout.setVisibility(View.GONE);

                    } else {
                        Toast.makeText(getContext(), getString(R.string.estimate_failed), Toast.LENGTH_LONG).show();

                    }
                }
            }
        }
    }


}
