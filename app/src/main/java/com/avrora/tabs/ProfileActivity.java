package com.avrora.tabs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.avrora.activity.AuthActivity;
import com.avrora.adapters.BonusesAdapter;
import com.avrora.app.R;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;
import static com.avrora.activity.AuthActivity.editor;
import static com.avrora.activity.TabsActivity.barcodeUrl;
import static com.avrora.activity.TabsActivity.getProfileEditor;
import static com.avrora.db.DB.readFromDBtoBonusArray;
import static com.avrora.utils.InternetCheck.isOnline;
import static com.avrora.utils.InternetCheck.showNoInternetDialog;
import static com.avrora.utils.Utility.checkFirstRun;


public class ProfileActivity extends Fragment {


    private ScrollView profileScrollView;
    private ExpandableHeightListView bonusListView;

    private ArrayList<HashMap<String, String>> bonusList;
    public TextView fullNameTxt;
    public TextView bonusCountTxt;
    public TextView cardNumderTxt;
    public TextView bonusAccrualsTxt;


    public ImageView barcodeImageView;


    private FrameLayout loader;
    public static Bitmap barcodeImage;


    private static final String TAG = "ProfileActivity";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_profile, container, false);

        profileScrollView = (ScrollView) view.findViewById(R.id.profile_scroll);

        bonusAccrualsTxt = (TextView) view.findViewById(R.id.bonus_accruals);


        fullNameTxt = (TextView) view.findViewById(R.id.user_name);

        cardNumderTxt = (TextView) view.findViewById(R.id.card_number_textview);

        bonusCountTxt = (TextView) view.findViewById(R.id.bonus);

        barcodeImageView = (ImageView) view.findViewById(R.id.barcode);

        bonusListView = (ExpandableHeightListView) view.findViewById(R.id.bonuses_listView);

        Button exitButton = (Button) view.findViewById(R.id.exitButton);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.clear();
                editor.commit();

                Intent intent = new Intent(getContext(), AuthActivity.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

                //getActivity().stopService(new Intent(getContext(),LocationService.class));

                Toast.makeText(getActivity(), getString(R.string.exit_from_app), Toast.LENGTH_LONG).show();


            }
        });


        loader = (FrameLayout) view.findViewById(R.id.loader_container);

        bonusList = new ArrayList<>();
        if (getContext() != null) {
            bonusList = readFromDBtoBonusArray(getContext());

        }

        if (!isOnline(getContext())) {
            showNoInternetDialog(getContext(), getActivity());
        } else {
            new DownloadImageFromInternet(barcodeImageView).execute(barcodeUrl);

            //new getProfileAsync().execute();
            // new getBonusListAsync().execute();
        }
        SharedPreferences prefs = getActivity().getSharedPreferences("PREF", MODE_PRIVATE);
        String preffullNameStr = prefs.getString("full_name", null);
        String preffirstNameStr = prefs.getString("first_name", null);
        String prefbonusCountStr = prefs.getString("bonus_cnt", null);
        String prefbarcodeUrl = prefs.getString("barcode", null);
        String prefcardNumberStr = prefs.getString("card_number", null);

        setBonusAdapter();
        setData(preffullNameStr, preffirstNameStr, prefbonusCountStr, prefcardNumberStr, prefbarcodeUrl);

        if (!checkFirstRun(getActivity())) {
            String imageStr = prefs.getString("image", null);
            barcodeImage = decodeBase64(imageStr);
            barcodeImageView.setImageBitmap(barcodeImage);

        }

        Log.d(TAG, "bonusList: " + bonusList);

        return view;

    }

    private void setBonusAdapter() {
        if (bonusList != null) {
            BonusesAdapter bonusesAdapter = new BonusesAdapter(ProfileActivity.this, bonusList);


            bonusListView.setAdapter(bonusesAdapter);

            bonusListView.setExpanded(true);

            bonusListView.setFocusable(false);

            profileScrollView.smoothScrollBy(0, 0);


        } else {
            bonusAccrualsTxt.setVisibility(View.GONE);

        }
    }


    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        DownloadImageFromInternet(ImageView imageView) {
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Log.d("imageURL: ", imageURL + "");
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bimage.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                getProfileEditor.putString("image", imageEncoded);

                getProfileEditor.commit();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            if (getActivity() != null && getContext() != null) {

                barcodeImage = result;
                imageView.setImageBitmap(result);
                //loader.setVisibility(View.GONE);
            }

        }
    }

    public void setData(String fullNameStr, String firstNameStr, String bonusCountStr, String cardNumberStr, String barcodeUrl) {
        Log.d("setData: ", barcodeUrl + "");
        Log.d("fullNameStr: ", firstNameStr + "|");


        firstNameStr = String.format("%s,", firstNameStr);

        Log.d("fullNameStr: ", firstNameStr + "");


        fullNameTxt.setText(firstNameStr);
        bonusCountTxt.setText(bonusCountStr);
        cardNumderTxt.setText(cardNumberStr);
    }


    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}