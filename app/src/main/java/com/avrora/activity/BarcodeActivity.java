package com.avrora.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.avrora.app.R;

import static com.avrora.tabs.ProfileActivity.barcodeImage;
import static com.avrora.utils.InternetCheck.showNoInternetDialog;
import static com.avrora.utils.InternetCheck.isOnline;

public class BarcodeActivity extends Activity {

    private FrameLayout loader;
    public  ImageView barcodeImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        barcodeImageView=(ImageView)findViewById(R.id.barcode);
        barcodeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        loader=(FrameLayout) findViewById(R.id.loader_container);

        loader.setVisibility(View.VISIBLE);

        if(isOnline(getApplicationContext())){
            loader.setVisibility(View.GONE);

            barcodeImageView.setImageBitmap(barcodeImage);

            //new DownloadImageFromInternet(barcodeImageView).execute(barcodeUrl);
        }else{
            showNoInternetDialog(this,BarcodeActivity.this);
            barcodeImageView.setImageBitmap(barcodeImage);


        }

    }

}
