package com.avrora.activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avrora.adapters.FragmentAdapterClass;
import com.avrora.app.BuildConfig;
import com.avrora.app.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import static com.avrora.activity.AuthActivity.IS_LOGIN;
import static com.avrora.activity.AuthActivity.USER_ID;
import static com.avrora.activity.AuthActivity.editor;
import static com.avrora.activity.AuthActivity.pref;
import static com.avrora.db.DB.writeToBonusTable;
import static com.avrora.db.DB.writeToShopsTable;
import static com.avrora.utils.Constants.GET_BONUS_LIST;
import static com.avrora.utils.Constants.GET_PROFILE;
import static com.avrora.utils.Constants.GET_SHOP_LIST;
import static com.avrora.utils.Constants.HOST;
import static com.avrora.utils.InternetCheck.isOnline;
import static com.avrora.utils.InternetCheck.showNoInternetDialog;
import static com.avrora.utils.Utility.md5Custom;

public class TabsActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private TextView tittleText;
    private TabLayout.Tab tab1;
    private TabLayout.Tab tab2;
    private TabLayout.Tab tab3;

    public String shopAddress = "";
    public String pushTextStr = "";
    public String timeOn = "";
    public String timeOff = "";


    public static double shopLat = 0;
    public static double shopLng = 0;

    public static String shopLatStr = "";
    public static String shopLngStr = "";
    public double distance = 0;

    private LocationManager locationManager;

    private FrameLayout loader;

    public String shopId = "";
    public static final String TAG = "TabsActivity";
    private FragmentAdapterClass fragmentAdapter;
    private CountDownLatch latch;

    private String fullNameStr = "";
    private String firstNameStr = "";
    private String bonusCountStr = "";
    private String cardNumberStr = "";
    private String version = "";
    private int reAuth = 0;
    public static int LOCATION_INTERVAL = 60 * 1000;
    public static String barcodeUrl = "";
    public static SharedPreferences.Editor getProfileEditor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_tabs);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        loader = (FrameLayout) findViewById(R.id.loader_container);


        getProfileEditor = TabsActivity.this.getSharedPreferences("PREF", MODE_PRIVATE).edit();


        if (!isOnline(TabsActivity.this)) {
            showNoInternetDialog(TabsActivity.this, TabsActivity.this);
        } else {
            main();

            String versionInGradle = BuildConfig.VERSION_NAME;

            if (!version.equals(versionInGradle)) {
                newVersionAlert();
            }
        }
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Message: ", "нельзя использовать местоположение");
        } else {
            if (!isServiceRunning()) {
                //checkLocation();
                //startService(new Intent(this,LocationService.class));
            }
        }


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout1);
        viewPager = (ViewPager) findViewById(R.id.pager1);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        tittleText = (TextView) toolbar.findViewById(R.id.toolbar_title);
        FrameLayout reloadViewContainer = (FrameLayout) toolbar.findViewById(R.id.refresh_icon_container);
        reloadViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplication(), TabsActivity.class);
                //finish();
                startActivity(i);

            }
        });

        FrameLayout barcodeViewContainer = (FrameLayout) toolbar.findViewById(R.id.barcode_icon_container);
        barcodeViewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TabsActivity.this, BarcodeActivity.class);
                startActivity(intent);


            }
        });

        tab1 = tabLayout.newTab().setText(R.string.profile_string).setIcon(R.drawable.profile_selected);
        tab2 = tabLayout.newTab().setText(R.string.mark_product_string).setIcon(R.drawable.product_mark);
        tab3 = tabLayout.newTab().setText(R.string.mark_shop_string).setIcon(R.drawable.shop_mark);

        tabLayout.addTab(tab1);
        tabLayout.addTab(tab2);
        tabLayout.addTab(tab3);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        fragmentAdapter = new FragmentAdapterClass(getSupportFragmentManager(), tabLayout.getTabCount());


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {
                switch (LayoutTab.getPosition()) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        tittleText.setText(R.string.profile_string);
                        tab1.setIcon(R.drawable.profile_selected);
                        tab2.setIcon(R.drawable.product_mark);
                        tab3.setIcon(R.drawable.shop_mark);

                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        tittleText.setText(R.string.mark_product_string);

                        tab1.setIcon(R.drawable.profile);
                        tab2.setIcon(R.drawable.product_mark_selected);
                        tab3.setIcon(R.drawable.shop_mark);

                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        tittleText.setText(R.string.mark_shop_string);

                        tab1.setIcon(R.drawable.profile);
                        tab2.setIcon(R.drawable.product_mark);
                        tab3.setIcon(R.drawable.shop_mark_selected);

                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });
        viewPager.setAdapter(fragmentAdapter);
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {
        if (exit) {
            moveTaskToBack(true);

        } else {
            Toast.makeText(this, R.string.back_exit,
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);

        }

    }

    public void main() {
        latch = new CountDownLatch(3);

        loader.setVisibility(View.VISIBLE);

        new GetProfileAsync().execute();
        new GetBonusListAsync().execute();
        new GetShopsAsync().execute();

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new Error("Main thread was interrupted");
        }
        loader.setVisibility(View.GONE);
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.avrora.service.LocationService".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.enable_location))
                .setMessage(R.string.enable_location_message)
                .setPositiveButton(getString(R.string.location_settings), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }


    private void newVersionAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.new_version_message)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        final String appPackageName = getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // todo do nothing, continue work
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {

        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private class GetShopsAsync extends AsyncTask<Void, Void, Void> {
        String error = "";


        @Override
        protected Void doInBackground(Void... voids) {

            HttpClient httpclient = new DefaultHttpClient();
            String url = String.format("user_id=%s&hash=%s", pref.getString(USER_ID, null), md5Custom(pref.getString(USER_ID, null)));

            HttpGet httpGet = new HttpGet(HOST + GET_SHOP_LIST + url);

            Log.d("shopURL: ", "httpGet " + HOST + GET_SHOP_LIST + url);

            try {
                HttpResponse response = httpclient.execute(httpGet);
                String responseString;
                HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
                    responseString = EntityUtils.toString(responseEntity);
                    Log.d("responseString:", responseString);
                    JSONObject jObject = new JSONObject(responseString);
                    String shopList = jObject.getString("shop_list");
                    JSONArray jsonArray = new JSONArray(shopList);
                    //clearShopsDB();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);

                        shopId = c.getString("id");
                        shopAddress = c.getString("address");
                        shopLatStr = c.getString("lat");
                        shopLatStr = c.getString("lng");
                        //shopLat=c.getDouble("lat");
                        //shopLng=c.getDouble("lng");
                        distance = c.getDouble("distance");
                        pushTextStr = c.getString("push_text");
                        timeOn = c.getString("time_on");
                        timeOff = c.getString("time_off");

                        if (getApplicationContext() != null) {

                            writeToShopsTable(getApplicationContext(), shopId, shopAddress, shopLatStr,
                                    shopLngStr, String.valueOf(distance), pushTextStr, timeOn, timeOff);
                            /*writeToShopsTable(getApplicationContext(), shopId, shopAddress, String.valueOf(shopLat),
                                    String.valueOf(shopLng), String.valueOf(distance), pushTextStr, timeOn, timeOff);*/
                        }
                    }

                }
                int status = response.getStatusLine().getStatusCode();
                Log.d("Status:", String.valueOf(status));

                Log.d("Http Post Response:", String.valueOf(response));
            } catch (org.apache.http.conn.HttpHostConnectException | org.apache.http.conn.ConnectTimeoutException e) {
                error = getString(R.string.server_error);

            } catch (IOException | JSONException e) {
                //z Toast.makeText(getApplication(),getString(R.string.json_parse_error),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getApplication() != null) {

                if (error.equals(getString(R.string.server_error))) {
                    Toast.makeText(TabsActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                } else {
                    //loader.setVisibility(View.GONE);
//                    viewPager.setAdapter(fragmentAdapter);
                }

            }
        }
    }

    private class GetBonusListAsync extends AsyncTask<Void, Void, Void> {
        String error = "";

        @Override
        protected Void doInBackground(Void... voids) {


            HttpClient httpclient = new DefaultHttpClient();
            String url = String.format("user_id=%s&hash=%s", pref.getString(USER_ID, null), md5Custom(pref.getString(USER_ID, null)));

            HttpGet httpGet = new HttpGet(HOST + GET_BONUS_LIST + url);
            Log.d(TAG, "httpGet " + HOST + GET_BONUS_LIST + url);

            try {
                HttpResponse response = httpclient.execute(httpGet);
                String responseString;
                HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
                    responseString = EntityUtils.toString(responseEntity);
                    Log.d("responseString:", responseString);
                    JSONObject jObject = new JSONObject(responseString);
                    String bonusList = jObject.getString("bonus_list");
                    JSONArray jsonArray = new JSONArray(bonusList);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject c = jsonArray.getJSONObject(i);

                        String date = c.getString("date");
                        String sum = c.getString("sum");

                        if (getApplicationContext() != null) {
                            writeToBonusTable(getApplicationContext(), date, sum);

                        }

                    }
                }
                int status = response.getStatusLine().getStatusCode();
                Log.d("Status:", String.valueOf(status));

                Log.d("Http Post Response:", String.valueOf(response));
            } catch (org.apache.http.conn.HttpHostConnectException | org.apache.http.conn.ConnectTimeoutException e) {
                error = getString(R.string.server_error);

            } catch (IOException | JSONException e) {
                //z Toast.makeText(getApplication(),getString(R.string.json_parse_error),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //setBonusAdapter();
            //loader.setVisibility(View.GONE);

            if (getApplication() != null) {
                if (error.equals(TabsActivity.this.getString(R.string.server_error))) {
                    Toast.makeText(getApplicationContext(), getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    private class GetProfileAsync extends AsyncTask<Void, Void, Void> {
        String error = "";

        @Override
        protected Void doInBackground(Void... voids) {
            HttpClient httpclient = new DefaultHttpClient();

            String url = String.format("user_id=%s&hash=%s", pref.getString(USER_ID, null), md5Custom(pref.getString(USER_ID, null)));

            HttpGet httpGet = new HttpGet(HOST + GET_PROFILE + url);
            Log.d(TAG, "httpGet " + HOST + GET_PROFILE + url);

            try {
                HttpResponse response = httpclient.execute(httpGet);
                String responseString = "--";
                HttpEntity responseEntity = response.getEntity();
                if (responseEntity != null) {
                    responseString = EntityUtils.toString(responseEntity);
                    Log.d("responseString:", responseString);
                    JSONObject jObject = new JSONObject(responseString);
                    String responseProfileData = jObject.getString("status");
                    fullNameStr = jObject.getString("full_name");
                    firstNameStr = jObject.getString("first_name");
                    Log.d("firstNameStrFirst:", firstNameStr + "|");

                    bonusCountStr = jObject.getString("bonus_cnt");
                    barcodeUrl = jObject.getString("barcode");
                    cardNumberStr = jObject.getString("card_number");
                    reAuth = jObject.getInt("re_auth");
                    version = jObject.getString("version");
                    LOCATION_INTERVAL = jObject.getInt("geo_time");

                    Log.d("responseProfileData:", responseProfileData);
                    Log.d("barcodeUrl:", barcodeUrl);


                    {
                        getProfileEditor.putString("full_name", fullNameStr);
                        getProfileEditor.putString("first_name", firstNameStr);

                        getProfileEditor.putString("bonus_cnt", bonusCountStr);
                        getProfileEditor.putString("barcode", barcodeUrl);
                        getProfileEditor.putString("card_number", cardNumberStr);
                        getProfileEditor.putInt("geo_time", LOCATION_INTERVAL);

                        getProfileEditor.commit();
                    }
                }
                int status = response.getStatusLine().getStatusCode();
                Log.d("Status:", String.valueOf(status));

                Log.d("Http Post Response:", responseString);
            } catch (org.apache.http.conn.HttpHostConnectException | org.apache.http.conn.ConnectTimeoutException e) {
                error = getString(R.string.server_error);

            } catch (IOException | JSONException e) {
                //z Toast.makeText(getApplication(),getString(R.string.json_parse_error),Toast.LENGTH_LONG).show();
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getApplication() != null) {
                if (error.equals(getString(R.string.server_error))) {
                    Toast.makeText(TabsActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                } else {
                    if (reAuth == 1) {
                        editor.putBoolean(IS_LOGIN, false);
                        editor.commit();

                        Intent intent = new Intent(TabsActivity.this, AuthActivity.class);
                        startActivity(intent);
                    }
                    //new DownloadImageFromInternet(barcodeImageView).execute(barcodeUrl);

                    //setData(fullNameStr, firstNameStr, bonusCountStr, cardNumberStr, barcodeUrl);

                    getProfileEditor.putString("full_name", fullNameStr);
                    getProfileEditor.putString("first_name", firstNameStr);

                    getProfileEditor.putString("bonus_cnt", bonusCountStr);
                    getProfileEditor.putString("barcode", barcodeUrl);
                    getProfileEditor.putString("card_number", cardNumberStr);
                    getProfileEditor.putInt("geo_time", LOCATION_INTERVAL);

                    getProfileEditor.commit();
                    //loader.setVisibility(View.GONE);

                }
            }
        }
    }

    public static void sendNotification(Context context, String shopName, String timeOn, String timeOff) {
        Log.d("START NOTIFICATION", "==================================");


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date dateTimeOn = null;
        Date dateTimeOff = null;

        try {
            dateTimeOn = sdf.parse(timeOn);
            dateTimeOff = sdf.parse(timeOff);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "dateTimeOn: " + "hours: " + dateTimeOn.getHours() + ": minutes: " + dateTimeOn.getMinutes() + ": second: " + dateTimeOn.getSeconds());
        Log.d(TAG, "dateTimeOff: " + "hours: " + dateTimeOff.getHours() + ": minutes: " + dateTimeOff.getMinutes() + ": second: " + dateTimeOff.getSeconds());

        Calendar currentTime = Calendar.getInstance();
        Calendar openlTime = Calendar.getInstance();
        Calendar closedTime = Calendar.getInstance();

        openlTime.set(Calendar.HOUR_OF_DAY, dateTimeOn.getHours());
        openlTime.set(Calendar.MINUTE, dateTimeOn.getMinutes());
        openlTime.set(Calendar.SECOND, dateTimeOn.getSeconds());
        openlTime.set(Calendar.MILLISECOND, 0);

        closedTime.set(Calendar.HOUR_OF_DAY, dateTimeOff.getHours());
        closedTime.set(Calendar.MINUTE, dateTimeOff.getMinutes());
        closedTime.set(Calendar.SECOND, dateTimeOff.getSeconds());
        closedTime.set(Calendar.MILLISECOND, 0);


        // if(distanceBetween<30600){
        if (currentTime.after(openlTime) && currentTime.before(closedTime)) {
            //sendToday=true;

            Intent intent = new Intent(context, AuthActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            //Set sound of notification
            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("АВРОРА")
                    .setContentText(shopName)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setAutoCancel(true)
                    .setSound(notificationSound)
                    .setContentIntent(pendingIntent);


            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notifiBuilder.build());

            // Log.d(TAG,"sendToday: "+sendToday);
        }

        //  }

    }

}
