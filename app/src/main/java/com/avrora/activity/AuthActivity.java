package com.avrora.activity;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avrora.app.R;
import com.avrora.masked.MaskedEditText;
import com.avrora.smspin.PinEntryView;
import com.avrora.utils.InputValidator;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.avrora.utils.Constants.AUTHENTICATION;
import static com.avrora.utils.Constants.AUTORIZATION;
import static com.avrora.utils.Constants.HOST;
import static com.avrora.utils.InternetCheck.isOnline;
import static com.avrora.utils.InternetCheck.showNoInternetDialog;
import static com.avrora.utils.Utility.md5Custom;
import static com.avrora.utils.Utility.showSoftKeyboard;


public class AuthActivity extends Activity {

    public static final String PREF_NAME = "AndroidHivePref";
    public static final String IS_LOGIN = "IsLoggedIn";
    public static final String USER_ID = "userId";
    public static final String TAG = "AuthActivity";

    private final static int ALL_PERMISSIONS_RESULT = 1;

    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static int PRIVATE_MODE = 0;
    public String pinTest = "";
    private EditText cardNumberEditText;
    private EditText phoneNumberEditText;
    private MaskedEditText maskedPhoneNumberEditText;
    private MaskedEditText maskedCardNumberEditText;
    private LinearLayout edtxLayout;
    private LinearLayout attentionLayout;
    private LinearLayout pinCodeLayout;
    private String pinCode = "";
    private String cardNumber = "";
    private String phoneNumber = "";
    private String token = "";
    private String userID = "";
    private String responseStatusAuthorization = "";
    private String responseStatusAuthentication = "";
    private FrameLayout loader;
    private PinEntryView smsCode;

    private List<String> permissionsToRequest;
    private List<String> permissionsRejected = new ArrayList<>();

    private final List<String> PERMISSIONS = Arrays.asList(
            CAMERA,
            WRITE_EXTERNAL_STORAGE,
            READ_EXTERNAL_STORAGE
            // ACCESS_FINE_LOCATION,
            // ACCESS_COARSE_LOCATION
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        pref = getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
        editor.commit();

        checkLogin();

        Log.d(TAG, "userID" + pref.getString(USER_ID, null));
        loader = (FrameLayout) findViewById(R.id.loader_container);

        permissionsToRequest = findUnAskedPermissions(PERMISSIONS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }

        smsCode = (PinEntryView) findViewById(R.id.pin_sms);
        smsCode.setOnPinEnteredListener(new PinEntryView.OnPinEnteredListener() {
            @Override
            public void onPinEntered(String pin) {

                pinCode = pin;
                if (isOnline(getApplication())) {
                    new AuthenticationAsync().execute();
                } else {
                    showNoInternetDialog(AuthActivity.this, AuthActivity.this);
                }

            }
        });


        edtxLayout = (LinearLayout) findViewById(R.id.edit_text_layout);

        attentionLayout = (LinearLayout) findViewById(R.id.attention_layout);

        pinCodeLayout = (LinearLayout) findViewById(R.id.sms_code_layout);


        cardNumberEditText = (EditText) findViewById(R.id.card_number_edittext);
        cardNumberEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    cardNumberEditText.setVisibility(View.GONE);
                    maskedCardNumberEditText.setVisibility(View.VISIBLE);
                    maskedCardNumberEditText.requestFocus();
                    showSoftKeyboard(AuthActivity.this);
                }
            }
        });

        maskedCardNumberEditText = (MaskedEditText) findViewById(R.id.card_number_input);

        maskedCardNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    Log.d(TAG, "maskedPhoneNumberEditText");
                    return true;

                }
                return false;
            }
        });

        phoneNumberEditText = (EditText) findViewById(R.id.phone_number_edittext);

        phoneNumberEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    phoneNumberEditText.setVisibility(View.GONE);
                    maskedPhoneNumberEditText.setVisibility(View.VISIBLE);
                    maskedPhoneNumberEditText.requestFocus();
                }
            }
        });


        maskedPhoneNumberEditText = (MaskedEditText) findViewById(R.id.phone_input);

        maskedPhoneNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    Log.d(TAG, "maskedPhoneNumberEditText");
                    return true;

                }
                return false;
            }
        });

        Button backBtn = (Button) findViewById(R.id.backButton);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attentionLayout.setVisibility(View.GONE);
                edtxLayout.setVisibility(View.VISIBLE);
            }
        });
        Button continueBtn = (Button) findViewById(R.id.continueButton);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                token = FirebaseInstanceId.getInstance().getToken();
                Log.d(TAG, "Token: " + token);

                if (isOnline(getApplication())) {
                    submitAuthorization();
                } else {
                    showNoInternetDialog(AuthActivity.this, AuthActivity.this);
                }

            }
        });
        restorePinCodeActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
        editor.putBoolean("isPinCodeActivity", false);
        editor.commit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (edtxLayout.hasFocus()) {
                finish();
            }
            pinCodeLayout.setVisibility(View.GONE);
            attentionLayout.setVisibility(View.GONE);
            edtxLayout.setVisibility(View.VISIBLE);


            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void submitAuthorization() {
        phoneNumber = "0" + maskedPhoneNumberEditText.getRawText();
        cardNumber = "777000" + maskedCardNumberEditText.getRawText();

        Log.d(TAG, "CardNumber: " + cardNumber);
        Log.d(TAG, "PhoneNumber: " + phoneNumber);

        maskedCardNumberEditText.setError(null);
        maskedPhoneNumberEditText.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (cardNumberEditText.isCursorVisible()) {
            cardNumberEditText.setError(getString(R.string.error_field_required));
            cardNumberEditText.requestFocus();
        }
        if (phoneNumberEditText.isCursorVisible()) {
            phoneNumberEditText.setError(getString(R.string.error_field_required));
            phoneNumberEditText.requestFocus();
        }
        if (InputValidator.isFieldEmpty(phoneNumber)) {
            maskedPhoneNumberEditText.setError(getString(R.string.error_field_required));
            focusView = maskedPhoneNumberEditText;
            cancel = true;
        }
        if (phoneNumber.length() != 10) {
            maskedPhoneNumberEditText.setError(getString(R.string.error_field_required));
            focusView = maskedPhoneNumberEditText;
            cancel = true;
        }

        if (InputValidator.isFieldEmpty(cardNumber)) {
            maskedCardNumberEditText.setError(getString(R.string.error_field_required));
            focusView = maskedCardNumberEditText;
            cancel = true;
        }
        if (cardNumber.length() != 13) {
            maskedCardNumberEditText.setError(getString(R.string.error_field_required));
            focusView = maskedCardNumberEditText;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            loader.setVisibility(View.VISIBLE);
            new AuthorizationAsync().execute();

        }

    }

    public void checkLogin() {
        boolean isLoggedIn = pref.getBoolean(IS_LOGIN, false);

        if (isLoggedIn) {
            Intent intent = new Intent(getApplication(), TabsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    private ArrayList<String> findUnAskedPermissions(List<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle(R.string.permission_title)
                .setPositiveButton("ОK", okListener)
                .setNegativeButton("Відхилити", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel(getString(R.string.permission_message),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }

    }

    private void showPinCodeActivity() {
        edtxLayout.setVisibility(View.GONE);
        pinCodeLayout.setVisibility(View.VISIBLE);
        smsCode.setText("");
        smsCode.requestFocus();
        showSoftKeyboard(AuthActivity.this);
    }

    public void authorization() {
        if (responseStatusAuthorization.equals("ok")) {
            showPinCodeActivity();
        } else {
            edtxLayout.setVisibility(View.GONE);
            attentionLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("isPinCodeActivity", pinCodeLayout.getVisibility() == View.VISIBLE);
        outState.putString("userID", userID);
        outState.putString("pinCode", pinCode);

        editor.putBoolean("isPinCodeActivity", pinCodeLayout.getVisibility() == View.VISIBLE);
        editor.putString(USER_ID, userID);
        editor.commit();

        Log.d(TAG, String.format("onSaveInstanceState... isPinCodeActivity: %s; userID: %s; pinCode: %s;", pinCodeLayout.getVisibility() == View.VISIBLE, userID, pinCode));
    }

    private void restorePinCodeActivity() {
        Log.d(TAG, "restorePinCodeActivity...: ");

        boolean isPinCodeActivity = pref.getBoolean("isPinCodeActivity", false);
        userID = pref.getString(USER_ID, userID);;

        Log.d(TAG, String.format("restorePinCodeActivity... isPinCodeActivity: %s; userID: %s; pinCode: %s;", isPinCodeActivity, userID, pinCode));

        if (isPinCodeActivity) {
            showPinCodeActivity();
        }
    }


    public void authentication() {
        if (responseStatusAuthentication.equals("ok")) {
            editor.putBoolean(IS_LOGIN, true);
            editor.putString(USER_ID, userID);

            editor.commit();

            Intent intent = new Intent(AuthActivity.this, TabsActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(AuthActivity.this, getString(R.string.wrong_sms_pin), Toast.LENGTH_LONG).show();
        }

    }

    private class AuthenticationAsync extends AsyncTask<Void, Void, Void> {

        private String error = "";

        @Override
        protected Void doInBackground(Void... voids) {
            HttpClient httpclient = new DefaultHttpClient();
            String urlParams = String.format("user_id=%s&pin=%s&hash=%s", userID, pinCode, md5Custom(pinCode + userID));

            HttpGet httpGet = new HttpGet(HOST + AUTHENTICATION + urlParams);
            Log.d(TAG, "httpGet " + HOST + AUTHENTICATION + urlParams);

            try {
                HttpResponse response = httpclient.execute(httpGet);
                HttpEntity responseEntity = response.getEntity();

                if (responseEntity != null) {
                    String responseString = EntityUtils.toString(responseEntity);
                    Log.d("responseStringsAuthen:", responseString);

                    responseStatusAuthentication = new JSONObject(responseString).getString("status");

                    Log.d("responseStatusAuthen: ", responseStatusAuthentication);
                }
                int status = response.getStatusLine().getStatusCode();
                Log.d("Status:", String.valueOf(status));

            } catch (org.apache.http.conn.HttpHostConnectException | org.apache.http.conn.ConnectTimeoutException e) {
                error = getString(R.string.server_error);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (error.equals(getString(R.string.server_error))) {
                Toast.makeText(AuthActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                if (!responseStatusAuthentication.equals("")) {
                    authentication();
                }
            }
        }
    }

    private class AuthorizationAsync extends AsyncTask<Void, Void, Void> {

        private String error = "";

        @Override
        protected Void doInBackground(Void... voids) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(HOST + AUTORIZATION);
            Log.d(TAG, "httppost: " + HOST + AUTORIZATION);

            List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

            nameValuePair.add(new BasicNameValuePair("card_number", cardNumber));
            nameValuePair.add(new BasicNameValuePair("phone_number", phoneNumber));
            nameValuePair.add(new BasicNameValuePair("phone_id", token));
            nameValuePair.add(new BasicNameValuePair("hash", md5Custom(cardNumber + phoneNumber + token)));

            Log.d("mainToPost", "mainToPost" + nameValuePair.toString());

            try {
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePair, "UTF-8"));
                HttpResponse response = httpclient.execute(httppost);
                String responseString;
                HttpEntity responseEntity = response.getEntity();

                if (responseEntity != null) {
                    responseString = EntityUtils.toString(responseEntity);
                    Log.d("responseStringAuthoriz:", responseString);

                    JSONObject jObject = new JSONObject(responseString);
                    responseStatusAuthorization = jObject.getString("status");
                    userID = jObject.getString("id");
                    pinTest = jObject.getString("pin");

                    Log.d("responseStatus:", responseStatusAuthorization);
                    Log.d("userID:", userID);
                }

                int status = response.getStatusLine().getStatusCode();
                Log.d("Status:", String.valueOf(status));
                Log.d("Http Post Response:", String.valueOf(response));
            } catch (org.apache.http.conn.HttpHostConnectException | org.apache.http.conn.ConnectTimeoutException e) {
                e.printStackTrace();
                error = getString(R.string.server_error);
            } catch (JSONException e) {
                e.printStackTrace();
                error = getString(R.string.wrong_data);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.GONE);

            if (!error.isEmpty()) {
                if (error.equals(getString(R.string.server_error))) {
                    Toast.makeText(AuthActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
                if (error.equals(getString(R.string.wrong_data))) {
                    edtxLayout.setVisibility(View.GONE);
                    attentionLayout.setVisibility(View.VISIBLE);
                }
            } else {
                if (!responseStatusAuthorization.equals("") && !userID.equals("")) {
                    authorization();
                }
            }
        }
    }


}