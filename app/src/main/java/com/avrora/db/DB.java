package com.avrora.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by egornoskov on 22.05.17.
 */

public class DB {
    public static final String TAG="DB";
    public static ArrayList<HashMap<String, String>> shopArrayList;
    public static ArrayList<String> adressList;
    public static ArrayList<HashMap<String, String>> bonusArrayList;



    public static class DBShopsHelper extends SQLiteOpenHelper {


        public DBShopsHelper(Context context) {
            // конструктор суперкласса
            super(context, "shops", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(TAG, "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table shops ("
                    + "id primary key,"
                    + "address text,"
                    + "lat text,"
                    + "lng text,"
                    + "push_text text,"
                    + "time_on text,"
                    + "time_off text,"
                    + "distance text" + ");");

            db.execSQL("create table bonusList ("
                    + "date text primary key,"
                    + "sum text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public static void writeToShopsTable(Context context,String id, String address, String lat, String lng,
                                      String distance,String pushText,String timeOn,String timeOff){

        DBShopsHelper dbHelper = new DBShopsHelper(context);

        // создаем объект для данных
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();


        cv.put("id",id);
        cv.put("address", address);
        cv.put("lat", lat);
        cv.put("lng", lng);
        cv.put("distance", distance);
        cv.put("push_text",pushText);
        cv.put("time_on",timeOn);
        cv.put("time_off",timeOff);


        //db.insert("shops", null, cv);
        db.replace("shops", null, cv);

        Log.d(TAG, "shops, row inserted, "+"; id: "+id+"; address: "+address+"; lat: "+lat+"; lng: "
                +lng+"; distance: "+distance+"; pushText: "+pushText+"; timeOn: "+timeOn+"; timeOff: "+timeOff);

    }


    /*public static  void clearShopsDB(){

        int clearCount = db.delete("shops", null, null);
        Log.d(TAG, "deleted rows count = " + clearCount);
        //dbHelper.close();
    }*/
    public static ArrayList<HashMap<String, String>>  readFromDBtoShopArray(Context context){
        Log.d(TAG, "--- Rows in mytable: ---");
        DBShopsHelper dbHelper = new DBShopsHelper(context);
        shopArrayList = new ArrayList<>();


        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.query("shops", null, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int id = c.getColumnIndex("id");
            int address = c.getColumnIndex("address");
            int lat = c.getColumnIndex("lat");
            int lng = c.getColumnIndex("lng");
            int distance = c.getColumnIndex("distance");
            int timeOn=c.getColumnIndex("time_on");
            int timeOff=c.getColumnIndex("time_off");
            int pushText=c.getColumnIndex("push_text");


            shopArrayList = new ArrayList<>();



            do {
                HashMap<String, String > shops = new HashMap<>();

                shops.put("id", c.getString(id));
                shops.put("address", c.getString(address));
                shops.put("lat", c.getString(lat));
                shops.put("lng", c.getString(lng));
                shops.put("distance", c.getString(distance));
                shops.put("time_on",c.getString(timeOn));
                shops.put("time_off",c.getString(timeOff));
                shops.put("push_text",c.getString(pushText));


                shopArrayList.add(shops);

                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            Log.d(TAG, "0 rows");
        c.close();
        return shopArrayList;
    }


    public static ArrayList<String>  readFromDBtoAdressArray(Context context){
        Log.d(TAG, "--- Rows in mytable: ---");
        DBShopsHelper dbHelper = new DBShopsHelper(context);

        Log.d(TAG,"context: "+context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        Cursor c = db.query("shops", null, null, null, null, null, null);
        if (c.moveToFirst()) {

            int address = c.getColumnIndex("address");


            adressList = new ArrayList<>();



            do {
                adressList.add(c.getString(address));

            } while (c.moveToNext());
        } else
            Log.d(TAG, "0 rows");
        c.close();
        return adressList;
    }

    public static void writeToBonusTable(Context context,String date, String sum){

        DBShopsHelper dbHelper = new DBShopsHelper(context);

        // создаем объект для данных
        ContentValues cv = new ContentValues();
        // подключаемся к БД
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        cv.put("date",date);
        cv.put("sum", sum);

        db.replace("bonusList", null, cv);

        Log.d(TAG, "bonusList, row inserted, "+"; time: "+date);

    }

    public static ArrayList<HashMap<String, String>>  readFromDBtoBonusArray(Context context){
        Log.d(TAG, "--- Rows in mytable: ---");
        DBShopsHelper dbHelper = new DBShopsHelper(context);

        SQLiteDatabase db = dbHelper.getWritableDatabase();


        Cursor c = db.query("bonusList", null, null, null, null, null, null);
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int date = c.getColumnIndex("date");
            int sum = c.getColumnIndex("sum");


            bonusArrayList = new ArrayList<>();



            do {
                HashMap<String, String > bonus = new HashMap<>();

                bonus.put("date", c.getString(date));
                bonus.put("sum", c.getString(sum));


                bonusArrayList.add(bonus);

            } while (c.moveToNext());
        } else
            Log.d(TAG, "0 rows");
        c.close();
        return bonusArrayList;
    }
}
