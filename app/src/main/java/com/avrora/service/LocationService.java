package com.avrora.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.Calendar;

import static com.avrora.activity.TabsActivity.LOCATION_INTERVAL;
import static com.avrora.activity.TabsActivity.sendNotification;
import static com.avrora.db.DB.readFromDBtoShopArray;
import static com.avrora.utils.Constants.LOCATION_DISTANCE;

/**
 * Created by egornoskov on 15.04.17.
 */

public class LocationService extends Service
{
    private static final String TAG = "BOOMBOOMTESTGPS";
    private LocationManager mLocationManager = null;

    public static double longitude;
    public static double latitude;
    private SQLiteDatabase db;
    public static boolean sendToday=false;





    private class LocationListener implements android.location.LocationListener
    {
        Location mLastLocation;

        LocationListener(String provider)
        {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }


        @Override
        public void onLocationChanged(Location location)
        {
            Calendar currentTime = Calendar.getInstance();
            Calendar dayStartRefresh = Calendar.getInstance();
            Calendar dayStopRefresh = Calendar.getInstance();


            dayStartRefresh.set(Calendar.HOUR_OF_DAY, 0);
            dayStartRefresh.set(Calendar.MINUTE, 0);
            dayStartRefresh.set(Calendar.SECOND, 0);
            dayStartRefresh.set(Calendar.MILLISECOND, 0);

            dayStopRefresh.set(Calendar.HOUR_OF_DAY, 0);
            dayStopRefresh.set(Calendar.MINUTE, 20);
            dayStopRefresh.set(Calendar.SECOND, 0);
            dayStopRefresh.set(Calendar.MILLISECOND, 0);

            if(currentTime.after(dayStartRefresh)&&currentTime.before(dayStopRefresh)){
                sendToday=false;

            }

            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            longitude=location.getLongitude();
            latitude=location.getLatitude();


            Log.d(TAG,"readFromDBtoShopArray(getApplicationContext()): "+readFromDBtoShopArray(getApplicationContext()));
            if(getApplicationContext()!=null&&readFromDBtoShopArray(getApplicationContext())!=null) {

                for (int i = 0; i < readFromDBtoShopArray(getApplicationContext()).size(); i++) {

                    double lat1 = Double.parseDouble(readFromDBtoShopArray(getApplicationContext()).get(i).get("lat"));
                    double lng1 = Double.parseDouble(readFromDBtoShopArray(getApplicationContext()).get(i).get("lng"));

                    //вулиця Івана Мазепи, 8
                    //49.565507, 34.514600
                    String pushText=readFromDBtoShopArray(getApplicationContext()).get(i).get("push_text");
                    String timeOn=readFromDBtoShopArray(getApplicationContext()).get(i).get("time_on");
                    String timeOff=readFromDBtoShopArray(getApplicationContext()).get(i).get("time_off");
                    double distance= Double.parseDouble(readFromDBtoShopArray(getApplicationContext()).get(i).get("distance"));


                    Log.d(TAG,"sendToday: "+sendToday);
                    Log.d(TAG,"timeOn: "+timeOn+"; timeOff: "+timeOff);
                    Log.d(TAG,"distFrom: "+distFrom(lat1, lng1, latitude, longitude));
                    Log.d(TAG,"distance: "+distance);

                    if(distFrom(lat1, lng1, latitude, longitude)<distance&&!sendToday){
                        sendNotification( getApplicationContext(),pushText,timeOn,timeOff);

                    }
                }
            }
        }


        @Override
        public void onProviderDisabled(String provider)
        {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider)
        {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0)
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate()
    {

        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy()
    {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }


    public static double distFrom(double lat1, double lng1, double lat2, double lng2) {


       double earthRadius = 6371000; //meters
       double dLat = Math.toRadians(lat2-lat1);
       double dLng = Math.toRadians(lng2-lng1);
       double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                       Math.sin(dLng/2) * Math.sin(dLng/2);
       double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
       double dist = (float) (earthRadius * c);
       Log.d(TAG,"dif dist: "+"lat 1: "+ lat1+"; lng 1:"+lng1);
       Log.d(TAG,"dif dist: "+"lat 2: "+ lat2+"; lng 2:"+lng2);


       return dist;
   }
}